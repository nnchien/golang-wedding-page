package routers

import (
	"gitlab.com/golang-wedding-page/controllers"
	"github.com/astaxie/beego"
)

func init() {
    	beego.Router("/", &controllers.MainController{})
	beego.SetStaticPath("/image","views/imgage")
	beego.SetStaticPath("/img","views/img")
	beego.SetStaticPath("/css","views/css")
	beego.SetStaticPath("/js","views/js")
	beego.SetStaticPath("/style.css", "views/style.css")
}
