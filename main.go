package main

import (
	_ "gitlab.com/golang-wedding-page/routers"
	"github.com/astaxie/beego"
)

func main() {
	beego.Run()
}

